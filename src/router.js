import Vue from 'vue'
import Router from 'vue-router'

function asyncComponent (name, loader) {
  return Vue.component(
    name,
    function (ok) {
      loader(ok)
    }
  )
}
const Home = asyncComponent('Home', function (ok) {
  return require(['./pages/Home'], ok)
})

const Fa = asyncComponent('Fa', function (ok) {
  return require(['./pages/Fa'], ok)
})
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/fa',
      name: 'Fa',
      component: Fa
    }
  ]
})
